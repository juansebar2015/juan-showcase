//
//  ViewController.swift
//  juan-showcase
//
//  Created by Juan Ramirez on 9/28/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Firebase

class ViewController: UIViewController {

    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        // If FIREBASE Auth ID Key exists then log user in
        if NSUserDefaults.standardUserDefaults().valueForKey(KEY_UID) != nil {
            self.performSegueWithIdentifier(SEGUE_LOGGED_IN, sender: nil)
        }
    }
    
    @IBAction func fbButtonPressed(sender: UIButton!) {
        let facebookLogin = FBSDKLoginManager()
        
        facebookLogin.logInWithReadPermissions(["email"]) { (facebookResult: FBSDKLoginManagerLoginResult!, facebookError: NSError!) in
            
            if facebookError != nil {
                print("Facebook login failed. Error \(facebookError)")
            } else {
                let accessToken = FBSDKAccessToken.currentAccessToken().tokenString
                print("Succesfully logged in with facebook. \(accessToken)")

                let credential = FIRFacebookAuthProvider.credentialWithAccessToken(accessToken)
                
                //Sign in user to FIREBASE
                FIRAuth.auth()?.signInWithCredential(credential, completion: { (user, error) in
                    
                    if error != nil {
                        print("Login failed. \(error)")
                    } else {
                        if user != nil {
                            print("Logged In! \(user!.email!)")
                            
                            let FIRuser = ["provider": credential.provider, "blah":"test"]
                            DataService.dataService.createFirebaseUser((user?.uid)!, user: FIRuser)
                            
                            NSUserDefaults.standardUserDefaults().setValue(user?.uid, forKey: KEY_UID)
                            self.performSegueWithIdentifier(SEGUE_LOGGED_IN, sender: nil)
                        }
                    }
                    
                })
                
                
                
//                FIRAuth.auth()!.signInWithCredential(credential, completion: { (user, error) in
//                    
//                    if error != nil {
//                        print("Login failed. \(error?.localizedDescription)")
//                    } else {
//                        print("Logged in. \(user)")
//                                                  //Facebook UserID      //Firebase UserID
//                        let userData = ["provider" : credential.provider]       //user?.providerID]
//                        DataService.dataService.createFirebaseUser((user?.uid)!, user: userData)
//                        
//                        NSUserDefaults.standardUserDefaults().setValue(user!.uid, forKey: KEY_UID)
//                        self.performSegueWithIdentifier(SEGUE_LOGGED_IN, sender: nil)
//                    }
//                })
        
            }
        }
    }
    
    @IBAction func attemptLogin(sender: UIButton!) {
        if let email = emailField.text where email != "", let password = passwordField.text where password != "" {
            
            FIRAuth.auth()?.signInWithEmail(email, password: password, completion: { (user, error) in
                
                if error != nil {
                    
                    print(error)
                    
                    if error?.code == STATUS_ACCOUNT_NONEXIST {
                        //Create a user
                        FIRAuth.auth()?.createUserWithEmail(email, password: password, completion: { (user, error) in
                            
                            if error != nil {
                                self.showErrorAlert("Could not create account", message: "Problem creating account. Try something else")
                            } else {
                                //Store the new user ID
                                NSUserDefaults.standardUserDefaults().setValue(user?.uid, forKey: KEY_UID)
                                
                                //Sign-in User
                                FIRAuth.auth()?.signInWithEmail(email, password: password, completion: { (user, error) in
                                    //Add code to handle any errors at login of newly created account
                                    let FIRuser = ["provider": (user?.providerID)!, "blah": "emailtexst"]
                                    
                                    DataService.dataService.createFirebaseUser((user?.uid)!, user: FIRuser)
                                    
                                })
                                self.performSegueWithIdentifier(SEGUE_LOGGED_IN, sender: nil)
                            }
                        })
                    } else if error?.code == STATUS_INVALID_PASSWORD {
                        self.showErrorAlert("Could not Login", message: "Please checck your username or password and try again!")
                    }
                    
                } else {
                    //Save current user logged in
                    NSUserDefaults.standardUserDefaults().setValue(user?.uid, forKey: KEY_UID)
                    self.performSegueWithIdentifier(SEGUE_LOGGED_IN, sender: nil)
                }
                
            })
            
            
        } else {
            showErrorAlert("Email and Password Required", message: "You must enter an email and a password")
        }
    }
    
    func showErrorAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        let action = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alert.addAction(action)
        
        presentViewController(alert, animated: true, completion: nil)
    }
    
}

