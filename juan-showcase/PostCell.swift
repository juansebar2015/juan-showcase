//
//  PostCell.swift
//  juan-showcase
//
//  Created by Juan Ramirez on 10/6/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import UIKit
import Alamofire
import Firebase

class PostCell: UITableViewCell {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var showcaseImage: UIImageView!
    @IBOutlet weak var descriptionText: UITextView!
    @IBOutlet weak var likesLabel: UILabel!
    @IBOutlet weak var likeImage: UIImageView!
    
    var post: Post!
    var request: Request?   // store request to be able to cancel in later time
    var likeRef : FIRDatabaseReference!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let tap = UITapGestureRecognizer(target: self, action: Selector("likedTapped:"))
        tap.numberOfTapsRequired = 1
        likeImage.addGestureRecognizer(tap)
        likeImage.userInteractionEnabled = true
    }
    
    override func drawRect(rect: CGRect) {
        //Do it here because it does have any constraints to a particular size so must do it over here
        profileImage.layer.cornerRadius = profileImage.frame.size.width / 2
        profileImage.clipsToBounds = true
        
        //Make image appear only to the specified bounds
        showcaseImage.clipsToBounds = true
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    // Configure the contents of the post
    func configureCell(post: Post, img: UIImage?) {
        self.post = post
        likeRef = DataService.dataService.REF_USER_CURRENT.child("likes").child(post.postKey)
        
        
        // Reset everything since the cell that disappears from view is being re-used
        self.descriptionText.text = post.postDescription
        self.likesLabel.text = "\(post.likes)"
        self.showcaseImage.hidden = false
        
        if post.imageUrl != nil {
            
            if img != nil {
                //Load image from the cache
                self.showcaseImage.image = img
            } else {
                // Make a request to get the image and then cache it                
                request = Alamofire.request(.GET, post.imageUrl!).validate(contentType: ["image/*"]).response(completionHandler: { (request, response, data, error) in
                    
                    if error == nil {
                        let img = UIImage(data: data!)!
                        self.showcaseImage.image = img
                        
                        //Cache image for future use
                        FeedVC.imageCache.setObject(img, forKey: self.post.imageUrl!)
                    } else {
                        print(error.debugDescription)
                    }
                    
                })
            }
            
        } else {
            self.showcaseImage.hidden = true
        }
        
        
        // Handle how to show the correct image for likes
        
        likeRef.observeSingleEventOfType(.Value, withBlock: { snapshot in
            
            if let doesNotExist = snapshot.value as? NSNull{
                // This means we have not liked this specific post
                self.likeImage.image = UIImage(named: "heart-empty")
            } else {
                self.likeImage.image = UIImage(named: "heart-full")
            }
            
        })
        
    }
    
    func likedTapped(sender: UITapGestureRecognizer) {
        likeRef.observeSingleEventOfType(.Value, withBlock: { snapshot in
            
            if let doesNotExist = snapshot.value as? NSNull{
                // This means we have not liked this specific post
                self.likeImage.image = UIImage(named: "heart-full")
                self.post.adjustLikes(true)
                self.likeRef.setValue(true)
                
            } else {
                self.likeImage.image = UIImage(named: "heart-empty")
                self.post.adjustLikes(false)
                self.likeRef.removeValue()  // Removes entire key
            }
            
        })
    }
}































