//
//  Post.swift
//  juan-showcase
//
//  Created by Juan Ramirez on 10/7/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import Foundation
import Firebase


class Post {
    private var _postDescription: String!
    private var _imageUrl: String?
    private var _likes: Int!
    private var _username: String!
    private var _postKey: String!
    
    private var _postRef: FIRDatabaseReference!
    
    var postDescription: String {
        return _postDescription
    }
    
    var imageUrl: String? {
        get{
            if _imageUrl == nil {
                return nil
            } else{
                return _imageUrl
            }
        }
    }
    
    var likes: Int {
        return _likes
    }
    
    var username: String {
        return _username
    }
    
    var postKey: String {
        return _postKey
    }
    
    init(description: String, imageUrl: String?, username: String) {
        self._postDescription = description
        self._imageUrl = imageUrl
        self._username = username
        
        //Generate a Key
        //let postKey = DataService.dataService.REF_POSTS.childByAutoId().key
    }
    
    init(postKey: String, dictionary: Dictionary<String, AnyObject>) {
        self._postKey = postKey
        
        if let likes = dictionary["likes"] as? Int {
            self._likes = likes
        }
        
        if let imgUrl = dictionary["imageUrl"] as? String {
            self._imageUrl = imgUrl
        }
        
        if let desc = dictionary["description"] as? String {
            self._postDescription = desc
        }
        
        // Get the reference of the current post
        self._postRef = DataService.dataService.REF_POSTS.child(self._postKey)
    }
    
    func adjustLikes(addLike: Bool) {
        if addLike {
            _likes = _likes + 1
        } else {
            _likes = _likes - 1
        }
        
        // Update the value of the likes
        _postRef.child("likes").setValue(_likes)
    }
}