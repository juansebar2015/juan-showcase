//
//  FeedVC.swift
//  juan-showcase
//
//  Created by Juan Ramirez on 10/6/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import UIKit
import Firebase
import Alamofire

class FeedVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var postField: MaterialsTextField!
    @IBOutlet weak var cameraImage: UIImageView!
    
    var imagePicker: UIImagePickerController!
    var posts = [Post]()
    static var imageCache = NSCache()
    var imageSelected = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 360
        
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        // Called anytime that data changes in Firebase "posts" object
        DataService.dataService.REF_POSTS.observeEventType(FIRDataEventType.Value, withBlock: { (snapshot) in
            
            //Clear data to be replaced with new
            self.posts = []
            
            // Gets all the post objects as an array
            if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
                
                for snap in snapshots {
                    //print("SNAP: \(snap)")
                    
                    if let postDict = snap.value as? Dictionary<String, AnyObject> {
                        let key = snap.key
                        
                        let post = Post(postKey: key, dictionary: postDict)
                        self.posts.append(post)
                    }
                }
                
            }
            
            self.tableView.reloadData()
        })
    }

    
    
    //Protocols
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let post = posts[indexPath.row]
        
        if let cell = tableView.dequeueReusableCellWithIdentifier("PostCell") as? PostCell {
            
            //For the new re-used cell cancel any existing request to not place the wrong image in the cell and be more efficient
            cell.request?.cancel()
            
            var image: UIImage?
            
            if let url = post.imageUrl {
                // Check if image is in cache
                image = FeedVC.imageCache.objectForKey(url) as? UIImage
            }
            
            cell.configureCell(post, img: image)
            
            return cell
        } else{
            return PostCell()
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let post = posts[indexPath.row]
        
        if post.imageUrl == nil {
            return UITableViewAutomaticDimension
        } else {
            return tableView.estimatedRowHeight
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        imagePicker.dismissViewControllerAnimated(true, completion: nil)
        cameraImage.image = image
        imageSelected = true
    }
    
    @IBAction func selectImage(sender: UITapGestureRecognizer) {
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func makePost(sender: AnyObject) {
        
        if let txt = postField.text where txt != "" {
            
            if let img = cameraImage.image where imageSelected == true {
                let urlStr = "https://post.imageshack.us/upload_api.php"
                let url = NSURL(string: urlStr)!
                
                //Convert image to JPEG and compress it
                let imageData = UIImageJPEGRepresentation(img, 0.2)! //0-> fully compressed, 1-> Not compressed
                
                //All of the requests Alamofire uploads must be in data
                let keyData = "12DJKPSU5fc3afbd01b1630cc718cae3043220f3".dataUsingEncoding(NSUTF8StringEncoding)!
                let keyJSON = "json".dataUsingEncoding(NSUTF8StringEncoding)!
                
                Alamofire.upload(.POST, url, multipartFormData: { multipartFormData in
                    
                    //Add the fields that the POST request is going to require
                    multipartFormData.appendBodyPart(data: imageData, name: "fileupload", fileName: "image", mimeType: "image/jpg")
                    multipartFormData.appendBodyPart(data: keyData, name: "key")
                    multipartFormData.appendBodyPart(data: keyJSON, name: "format")
                        //**** Just finished making the request
                    
                }) { encondingResult in
                    //Goes in when upload is done... Results will show up right here
                    switch encondingResult {
                    
                    case .Success(let upload, _,_):
                        //Get response data so we know how to store it
                        upload.responseJSON(completionHandler: { (response) in
                            
                            if let info = response.result.value as? Dictionary<String, AnyObject> {
                                
                                if let links = info["links"] as? Dictionary<String, AnyObject> {
                                    
                                    if let imageLink = links["image_link"] as? String {
                                        
                                        //Save to Firebase
                                        print("LINK: \(imageLink)")
                                        self.postToFirebase(imageLink)
                                    }
                                }
                            }
                        })
                    case .Failure(let error):
                        print(error)
                    }
                }
            } else {
                //No image selected thus send nil
                self.postToFirebase(nil)
            }
        }
    }
    
    func postToFirebase(imgUrl: String?) {
        
        let ref = FIRDatabase.database().reference()
        let key = ref.child("posts").childByAutoId().key
        
        var post: Dictionary<String, AnyObject> = [
            "description": postField.text!,
            "likes": 0
        ]
        
        if imgUrl != nil {
            post["imageUrl"] = imgUrl!
        }
        
        //My method
        /*
            • Make a new post in Firebase in Posts object in "posts/(key)"
            • Make a reference to the post in Users object the "users/(uid)/posts/(key)"
         */
//        let childUpdates = ["/posts/\(key)" : post, "/users/\(FIRAuth.auth()?.currentUser?.uid)/posts/\(key)": "true"]
//        
//        ref.updateChildValues(childUpdates as [NSObject : AnyObject])
        
        
        //Marks Method
        let firebasePost = DataService.dataService.REF_POSTS.childByAutoId()
        firebasePost.setValue(post)
        
        postField.text = ""
        cameraImage.image = UIImage(named: "camera")
        imageSelected = false
        
        self.tableView.reloadData()
    }
}
