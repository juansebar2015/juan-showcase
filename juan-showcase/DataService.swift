//
//  DataService.swift
//  juan-showcase
//
//  Created by Juan Ramirez on 9/30/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

//Singleton

import Foundation
import Firebase

//let URL_BASE = FIRDatabase.database().reference()

class DataService {
    
    static let dataService = DataService()
    
    // Interact with firebase
    private var _REF_BASE = FIRDatabase.database().reference()
    private var _REF_POSTS = FIRDatabase.database().reference().child("posts")
    private var _REF_USERS = FIRDatabase.database().reference().child("users")
 
    var REF_BASE: FIRDatabaseReference {
        return _REF_BASE
    }
    
    var REF_POSTS: FIRDatabaseReference {
        return _REF_POSTS
    }
    
    var REF_USERS: FIRDatabaseReference {
        return _REF_USERS
    }
    
    var REF_USER_CURRENT: FIRDatabaseReference {
        let uid = NSUserDefaults.standardUserDefaults().valueForKey(KEY_UID) as! String
        let user = REF_BASE.child("users").child(uid)
        
        return user
    }
    
//     var REF_USERS_CURRENT: FIRDatabaseReference {
//        let uid = NSUserDefaults.standardUserDefaults().valueForKey(KEY_UID) as! String
//        let user = URL_BASE.childByAppendingPath("users").child(uid)
//        return user!
//    }
 
//    func createFirebaseUser(uid: String, user: Dictionary<String, String>) {
//        REF_USERS.child(uid).updateChildValues(user)
//    }
    
    func createFirebaseUser(uid: String, user: Dictionary<String, String>) {
        // Save a new entry for a user if it doesnt exists
        REF_USERS.child(uid).setValue(user)
    }
    
}