//
//  MaterialsTextField.swift
//  juan-showcase
//
//  Created by Juan Ramirez on 9/29/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import UIKit

class MaterialsTextField: UITextField {

    override func awakeFromNib() {
        layer.cornerRadius = 2.0
        layer.borderColor = UIColor(red: SHADOW_COLOR, green: SHADOW_COLOR, blue: SHADOW_COLOR, alpha: 0.1).CGColor
        layer.borderWidth = 1.0
    }
    
    //For Placeholder
    
    // What is the bounds you want the text in
    override func textRectForBounds(bounds: CGRect) -> CGRect {
        //Moves the placeholder
        return CGRectInset(bounds, 10.0, 0.0)
    }
    
    //For editable text
    override func editingRectForBounds(bounds: CGRect) -> CGRect {
        //Moves the editable text
        return CGRectInset(bounds, 10.0, 0.0)
    }

}
